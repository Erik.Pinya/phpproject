<!doctype html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Profile</title>
</head>
<body>
  <h1 class="text-secondary border text-center">Profile</h1>
  <nav class="nav">
    <a class="nav-link active" href="profile.php">Profile</a>
    <a class="nav-link" href="init.php">Init</a>
    <a class="nav-link" href="transfer.php">Transfer</a>
    <a class="nav-link" href="logout.php">Logout</a>
  </nav>
  <form action="../Controller/controller.php" method="post">
    <div class="form-group col">
      <label for="userName">Nombre:</label>
      <input name="userName" type="text" class="form-control" >
    </div>

      <div class="form-group col">
      <label for="userSurname">Apellidos:</label>
      <input name="userSurname" type="text" class="form-control" >
    </div>
      <div class="form-group col">
        <label for="genre">Selecciona tu género:</label>
        <select class="form-control">
            <option>Hombre</option>
            <option>Mujer</option>
        </select>
    </div>

    <div class="form-group col">
      <label for="birthdate">Fecha de nacimiento:</label>
      <input name="birthdate" type="date" class="form-control">
    </div>

    <div class="form-group col">
      <label for="mobileNumber">Nº teléfono:</label>
      <input name="mobilePhone" type="tel" class="form-control" >
    </div>
    <div class="form-group col">
      <label for="email">Email:</label>
      <input name="email" type="email" class="form-control" >
    </div>
      <div class="form-group col">
      <label for="pass">Contraseña:</label>
      <input name="pass" type="password" class="form-control" >
    </div>
      <div class="form-group col">
        <label for="repeatPass">Repetir Contraseña:</label>
        <input name="repeatPass" type="password" class="form-control" >
    </div>
      <div class="form-group col">
        <input type="hidden" class="form-control" value="profile" name="control">
      </div>
    <div class="form-group col">
      <input type="submit" class="form-control btn btn-primary" name="submit" value="submit">
    </div>
  </form>
</body>
</html>