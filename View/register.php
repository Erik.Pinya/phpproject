<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Registro</title>
</head>

<body>
    <h1 class="text-secondary border text-center">Register</h1>

    <form action="../Controller/controller.php" method="post" class="container pt-1">

        <div class="form-group col">
            <label for="userName">Nombre:</label>
            <input name="userName" type="text" class="form-control" >
        </div>

        <div class="form-group col">
            <label for="userSurname">Apellidos:</label>
            <input name="userSurname" type="text" class="form-control" >
        </div>

        <div class="form-group col">
            <label for="genre">Género:</label>
            <select class="custom-select" name="genre">
                <option selected>Selecciona tu género</option>
                <option value="1">Hombre</option>
                <option value="2">Mujer</option>
                <option value="3">Helicoptero Apache de Guerra 1945</option>
            </select>
        </div>

        <div class="form-group col">
            <label for="birthdate">Fecha de nacimiento:</label>
            <input name="birthdate" type="date" class="form-control">
        </div>

        <div class="form-group col">
            <label for="dni">DNI:</label>
            <input name="dni" type="text" class="form-control">
        </div>

        <div class="form-group col">
            <label for="mobileNumber">Nº teléfono:</label>
            <input name="mobilePhone" type="tel" class="form-control">
        </div>

        <div class="form-group col">
            <label for="email">Email:</label>
            <input name="email" type="email" class="form-control">
        </div>

        <div class="form-group col">
            <label for="pass">Contraseña:</label>
            <input name="pass" type="password" class="form-control">
        </div>

        <div class="form-group col">
            <label for="repeatPass">Repite la contraseña:</label>
            <input name="repeatPass" type="password" class="form-control">
        </div>

        <div class="form-group col">
            <input type="hidden" class="form-control" value="register" name="control">
        </div>

        <div class="form-group col">
            <input type="submit" class="form-control btn btn-primary" name="submit" value="submit">
        </div>
    </form>

    <?php
    if (isset($_POST['message']))
        echo $_POST['message'] . '<br/>';
    ?>
</body>

</html>