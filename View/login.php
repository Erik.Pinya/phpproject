<!doctype html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Login</title>
</head>

<body>

  <h1 class="text-secondary border text-center">Login</h1>
  <form action="../Controller/controller.php" method="post" class="container pt-1">

      <div class="form-group col">
      <label for="userName">Nombre:</label>
      <input name="userName" type="text" class="form-control">
    </div>

    <div class="form-group col">
      <label for="pass">Contraseña:</label>
      <input name="pass" type="password" class="form-control">
    </div>

    <div class="form-group col">
      <input type="hidden" class="form-control" value="login" name="control">
    </div>

    <div class="form-group col">
      <input type="submit" class="form-control btn btn-primary" name="submit" value="submit">
    </div>
  </form>


  <?php
  if (isset($_POST['message']))
      echo $_POST['message'] . '<br/>';
  ?>
</body>

</html>