<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Transferir</title>
</head>

<body>
    <h1 class="text-secondary border text-center">Transfer</h1>
    <nav class="nav">
        <a class="nav-link" href="profile.php">Profile</a>
        <a class="nav-link" href="init.php">Init</a>
        <a class="nav-link active" href="transfer.php">Transfer</a>
        <a class="nav-link" href="logout.php">Logout</a>
    </nav>
    <form action="../Controller/controller.php" method="post">
        <div class="form-group col">
            <label for="accountNumber">IBAN:</label>
            <input name="accountNumber" type="text" class="form-control" placeholder="ES6651496819198985">
        </div>

        <div class="form-group col">
            <label for="amountOfMoney">Cantidad a Transferir:</label>
            <input name="amountOfMoney" type="text" class="form-control" placeholder="10.50€">
        </div>

        <div class="form-group col">
            <label for="annotation">Anotación:</label>
            <textarea name="annotation" id="annotationTransfer" cols="50" rows="2" placeholder="Pago Colegio "></textarea>
        </div>

        <div class="form-group col">
            <input type="hidden" class="form-control" value="transfer" name="control">
        </div>

        <div class="form-group col">
            <input type="submit" class="form-control btn btn-primary" name="submit" value="submit">
        </div>
    </form>


</body>

</html>

<?php
