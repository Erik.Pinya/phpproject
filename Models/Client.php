<?php


class Client
{
    private $id, $name, $birthdate, $surname, $genre, $email, $mobilePhone, $dni, $pass;

    /**
     * Client constructor.
     * @param $id integer
     * @param $name string
     * @param $birthdate string
     * @param $surname string
     * @param $genre boolean
     * @param $email string
     * @param $mobilePhone integer
     * @param $dni string
     * @param $pass string
     */
    public function __construct($id, $name, $birthdate, $surname, $genre, $email, $mobilePhone, $dni, $pass)
    {
        $this->id = $id;
        $this->name = $name;
        $this->birthdate = $birthdate;
        $this->surname = $surname;
        $this->genre = $genre;
        $this->email = $email;
        $this->mobilePhone = $mobilePhone;
        $this->dni = $dni;
        $this->pass = $pass;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getBirthdate()
    {
        return $this->birthdate;
    }

    /**
     * @param string $birthdate
     */
    public function setBirthdate($birthdate)
    {
        $this->birthdate = $birthdate;
    }

    /**
     * @return string
     */
    public function getSurname()
    {
        return $this->surname;
    }

    /**
     * @param string $surname
     */
    public function setSurname($surname)
    {
        $this->surname = $surname;
    }

    /**
     * @return bool
     */
    public function isGenre()
    {
        return $this->genre;
    }

    /**
     * @param bool $genre
     */
    public function setGenre($genre)
    {
        $this->genre = $genre;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return int
     */
    public function getMobilePhone()
    {
        return $this->mobilePhone;
    }

    /**
     * @param int $mobilePhone
     */
    public function setMobilePhone($mobilePhone)
    {
        $this->mobilePhone = $mobilePhone;
    }

    /**
     * @return string
     */
    public function getDni()
    {
        return $this->dni;
    }

    /**
     * @param string $dni
     */
    public function setDni($dni)
    {
        $this->dni = $dni;
    }

    /**
     * @return string
     */
    public function getPass()
    {
        return $this->pass;
    }

    /**
     * @param string $pass
     */
    public function setPass($pass)
    {
        $this->pass = $pass;
    }


}