<?php
require_once ('../Db/DBManager.php');
use DBManager;
function InsertCliente($cliente){
$manager=new DBManager();
try {
$sql="INSERT INTO cliente(nombre,apellidos,fecha_nacimiento,sexo,telefono,dni,email,password)";

    $nombre = $cliente->getNombre();
    $apellidos = $cliente->getApellidos();
    $fecha_nacimiento = $cliente->getFechaNacimiento();
    $sexo = $cliente->getSexo();
    $telefono = $cliente->getTelefono();
    $dni = $cliente->getDni();
    $email = $cliente->getEmail();
    $password = $cliente->getPassword();


    $stmt = $manager->getConexion()->prepare($sql);
    $stmt->bindParam(':nombre', $nombre);
    $stmt->bindParam(':apellidos', $apellidos);
    $stmt->bindParam(':fecha_nacimiento', $fecha_nacimiento);
    $stmt->bindParam(':sexo', $sexo);
    $stmt->bindParam(':telefono', $telefono);
    $stmt->bindParam(':dni', $dni);
    $stmt->bindParam(':email', $email);
    $stmt->bindParam(':password', $password);

    if ($stmt->execute()) {
        echo "todo ok";
    } else {
        echo "mal";

    }
}catch (PDOException $e){
    echo $e->getMessage();
}
}