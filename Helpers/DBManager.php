<?php
require_once('../Config/config.php');

class DBManager extends PDO{
    public $server = SERVER;
    public $user = USER;
    public $pass = PASS;
    public $port = PORT;
    public $db = DB;

    private $conexion;


    public function __construct(){
        $this -> conectar();
    }

    private final function conectar(){
        $conexion = null;

        try {
            if (is_array(PDO::getAvailableDrivers())){
                if (in_array("pgsql", PDO::getAvailableDrivers())){
                    $conexion = new PDO("pgsql:host=$this->server; $this->user; $this->pass; $this->port; $this->db");
                }else{
                    throw new PDOException("Error");
                }
            }
        }catch (PDOException $e){
            echo $e->getMessage();
        }
        $this->setConexion($conexion);
    }

    public final function setConexion($conexion){
        $this->conexion = $conexion;
    }

    public final function getConexion(){
        return $this->conexion;
    }

    public final function closeConexion(){
        $this->setConexion(null);
    }


}